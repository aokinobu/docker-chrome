FROM debian:stretch-slim
MAINTAINER Nobuyuki Aoki "aokinobu@gmail.com"

RUN apt-get update && \
  apt-get install -y wget gnupg && \
  apt-get clean -qq -y && \
  apt-get autoclean -qq -y && \
  apt-get autoremove -qq -y &&  \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /tmp/*
  
RUN wget -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y sudo fonts-takao-mincho libcanberra-gtk3-module wget fonts-liberation libappindicator1 libnspr4 libnss3 libx11-xcb1 libxss1 lsb-release xdg-utils \
  google-chrome-stable && \
  echo 'Cleaning up' && \
  apt-get clean -qq -y && \
  apt-get autoclean -qq -y && \
  apt-get autoremove -qq -y &&  \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /tmp/*
